# desktop-analog-clock

An elegant desktop analog clock with time zone choice, in Python 3.

## How to get

If you're running Debian 10 (+) on a 32 bits machine or Ubuntu 18.04 (+) on a 64 bits machine you may download one of the 2 standalone executable available in [Releases](https://codeberg.org/manuelcaeiro/desktop-analog-clock/releases).
Else, you can download and compile the source code or, easier, if you have Python 3 installed on your machine just download the Python file from the repository and run it.

## How to use

Run/execute the program and a little widget with a slider ranging from -12 to +12 will appear.

![widget with slider 0](https://codeberg.org/manuelcaeiro/desktop-analog-clock/raw/branch/main/pics/tkslider_widg.png)

Choose your timezone (you may check different time zones on this [World Time Zone Map](https://24timezones.com/timezone-map)).

![widget with slider -5](https://codeberg.org/manuelcaeiro/desktop-analog-clock/raw/branch/main/pics/tkslider2_widg.png)

 Close the widget and the clock will show with the hour of the time zone you chose.

 ![clock tz -5](https://codeberg.org/manuelcaeiro/desktop-analog-clock/raw/branch/main/pics/tzone-5_utctime-5.png)

**You can open several clocks with different time zone choices.**

 ## Know issues

 When you reopen the clock window after being minimized on the panel for a while, the seconds hand may appear jumpy or behind the digital count. This may easily be corrected by maximizing and restoring the window. 

 # Acknowledgements and Licences

 The Python script was written based on the [ANALOG CLOCK (PYTHON RECIPE)](code.activestate.com/recipes/578875-analog-clock/) created by Anton Vredegoor on Sat, 4 Jul 2009 (in Python 2). Thank you, Anton.

 Freely I recieved, freely I give.

 **Anyone may use these files acknowledging their authors' copyright and in compliance with their Licences.**

 Copyright [2021] [J. Manuel Caeiro D. P.]

 All the code in this repository (including the executable bundles) is under the MIT License.
You may obtain a copy of the MIT License [here](https://opensource.org/licenses/MIT)
