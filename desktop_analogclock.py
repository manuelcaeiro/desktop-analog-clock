import sys
from datetime import timedelta, datetime
from math import sin, cos, pi
from threading import Thread
from tkinter import *


class window_position:
    
    def __init__(self, world, view):
        self.world = world
        self.view = view
        x_min, y_min, x_max, y_max = self.world
        X_min, Y_min, X_max, Y_max = self.view
        f_x = float(X_max - X_min) / float(x_max - x_min)
        f_y = float(Y_max - Y_min) / float(y_max - y_min)
        self.f = min(f_x, f_y)
        x_c = 0.5 * (x_min + x_max)
        y_c = 0.5 * (y_min + y_max)
        X_c = 0.5 * (X_min + X_max)
        Y_c = 0.5 * (Y_min + Y_max)
        self.c1 = X_c - self.f * x_c
        self.c2 = Y_c - self.f * y_c

    def __window_view(self, x, y):
        X = self.f * x + self.c1
        Y = self.f * -y + self.c2
        return X, Y

    def window_view(self, x1, y1, x2, y2):
        return self.__window_view(x1, y1), self.__window_view(x2, y2)

class make_thread(Thread):
    
    def __init__(self, func):
        Thread.__init__(self)
        self.__action = func
        self.debug = False

    def __del__(self):
        if self.debug:
            print("End Thread")

    def run(self):
        if self.debug:
            print("Begin Thread")
            self.__action()

class clock:

    def __init__(self, root, deltahours=0, w=300, h=300, useThread=False):
        self.world = [-1, -1, 1, 1]
        self.set_color()
        self.circlesize = 0.1
        self._ALL = "handles"
        self.root = root
        width, height = w, h
        self.pad = width/15
        self.root.bind("<Escape>", lambda _ : root.destroy())
        self.delta = timedelta(hours = deltahours)
        self.canvas = Canvas(root, width=width, height=height, background=self.bgcolor)
        view = (self.pad, self.pad, width-self.pad, height-self.pad)
        self.T = window_position(self.world, view)
        self.root.title("clock")
        self.canvas.bind("<Configure>", self.resize)
        self.canvas.pack(fill=BOTH, expand=YES)
        if useThread:
            st = make_thread(self.pace)
            st.debug = True
            st.start()
        else:
            self.pace()

    def resize(self, event):
        sc = self.canvas
        sc.delete(ALL)
        width = sc.winfo_width()
        height = sc.winfo_height()
        imgsize = min(width, height)
        self.pad = imgsize/15
        view = (self.pad, self.pad, width-self.pad, height-self.pad)
        self.T = window_position(self.world, view)
        self.canvas.create_rectangle([[0,0],[width,height]], fill=self.bgcolor)
        self.redraw()

    def set_color(self):
        self.bgcolor = "#000000"
        self.timecolor = "#ffffff"
        self.circlecolor = "#808080"

    def redraw(self):
        start = pi/2
        step = pi/6
        for i in range(12):
            angle = start -i * step
            x, y = cos(angle), sin(angle)
            self.paintcircle(x, y)
        self.painthms()
        self.paintcircle(0,0)

    def painthms(self):
        self.canvas.delete(self._ALL)
        T = datetime.timetuple(datetime.utcnow() - self.delta)
        x, x, x, h, m, s, x, x, x = T
        self.root.title('%02i:%02i:%02i' %(h,m,s))
        angle = pi/2 - pi/6 * (h + m/60.0)
        x, y = cos(angle) * 0.70, sin(angle) * 0.70
        scl = self.canvas.create_line
        # draw the hours handle
        scl(self.T.window_view(0,0,x,y), fill=self.timecolor, tag=self._ALL, width=self.pad/4)
        angle = pi/2 - pi/30 * (m + s/60.0)
        x, y = cos(angle) * 0.90, sin(angle) * 0.90
        # draw the minutes handle
        scl(self.T.window_view(0,0,x,y), fill=self.timecolor, tag=self._ALL, width=self.pad/5)
        angle = pi/2 - pi/30 * s
        x, y = cos(angle) * 0.95, sin(angle) * 0.95
        # draw the seconds handle
        scl(self.T.window_view(0,0,x,y), fill=self.timecolor, tag=self._ALL, arrow="last")

    def paintcircle(self, x, y):
        ss = self.circlesize / 2
        sco = self.canvas.create_oval
        sco(self.T.window_view(-ss+x, -ss+y, ss+x, ss+y), fill=self.circlecolor)

    def pace(self):
        self.redraw()
        self.root.after(200, self.pace)

def sel():
   selection = int(var.get())
   return selection

root = Tk()
var = DoubleVar()
scale = Scale( root, variable = var, from_=-12, to=12, orient=HORIZONTAL )
scale.pack(anchor=CENTER)
label = Label(root, text="Choose Time Zone \n& close this widget")
label.pack()
root.mainloop()

selection = sel()

def main():
    deltahours = -selection
    w = h = 300
    t = True

    root = Tk()
    root.geometry ('+0+0')    
    clock(root, deltahours, w, h, t)
    root.mainloop()

if __name__=='__main__':
    sys.exit(main())
